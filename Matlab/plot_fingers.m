
%% Plot offline content of matrix data (24*n)x3 (24 positions)
if ~exist('data', 'var')
    load('data_sample.mat');
end
close all
k = 1;
for i=1:length(data)
    p = data(i,:);
    if k < 25
        h(k) = trplot(transl(p(1),p(2),-p(3)), 'length', 0.005);
        if k == 1
            axis([-0.3 0.1 -0.2 0.2 -0.2 0.2]);
        end
        hold on
    else 
        h(k) = trplot(transl(p(1),p(2),-p(3)), 'handle', h(k-24), 'length', 0.005);
        hold on
    end
    k = k+1;
    pause(0.01)
    
    
end