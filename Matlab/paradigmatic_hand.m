
%% Create paradigmatic hand model
close all
hand = SGparadigmatic;

q = zeros(20,1);

hand = SGmoveHand(hand, q);

hand.F{4}.joints(5,1) = 2000;
hand.F{4}.joints(5,1)
SGplotHand(hand);

% for i=1:15
%     q(1) = q(1) + i;
%     hand = SGmoveHand(hand, q);
%     SGplotHand(hand);
%     pause(0.001)
% end