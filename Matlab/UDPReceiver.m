%% Disable firewall or allow localhost on port 8001
instrreset
warning('OFF');
close all
try
    clear t
end
% Create UDP connection
t = udpport("IPv4","LocalHost", "localhost","LocalPort",8001);
i = 1;
j = 1;
data = [];
while(1)
    A = read(t, 1, 'double');
    
   if not(isempty(A))
        A
        data(i,j) = A;
        j = j+1;
        if j==4
            j = 1;
            i = i+1; 
        end
   end
end
%remember to close the connection using clear t 