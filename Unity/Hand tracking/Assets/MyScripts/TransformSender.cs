﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

public class TransformSender : MonoBehaviour
{
	public OVRSkeleton skeleton;
	private List<OVRBone> fingerBones;

	StringBuilder sb = new StringBuilder(); //for log file

	//local host
	public string IP = "127.0.0.1";

	//Ports
	public int portLocal = 8000;
	public int portRemote = 8001;

	// Create necessary UdpClient objects
	UdpClient client;
	IPEndPoint remoteEndPoint;

	// Receiving Thread
	Thread receiveThread;
	// Message to be sent
	string strMessageSend = "";

	// info strings
	public string lastReceivedUDPPacket = "";
	public string allReceivedUDPPackets = "";
	// clear this from time to time!

	// start from Unity3d
	public void Start()
	{
		init();
		File.Delete("C:\\Users\\ricky\\Desktop\\NEU\\log.txt");
		StartCoroutine(DelayRoutine(2.5f));
	}
	public IEnumerator DelayRoutine(float delay)
	{
		yield return new WaitForSeconds(delay);
	}
	void Update()
	{
		fingerBones = new List<OVRBone>(skeleton.Bones);

		List<Vector3> fingerData = new List<Vector3>();

		foreach (var bone in fingerBones)
		{
			fingerData.Add(skeleton.transform.InverseTransformPoint(bone.Transform.position));
		}
		foreach (var finger in fingerData)
		{
			sendData(finger);
		}


	}

	// OnGUI
	void OnGUI()
	{
		Rect rectObj = new Rect(40, 10, 200, 400);
		GUIStyle style = new GUIStyle();
		style.alignment = TextAnchor.UpperLeft;
		GUI.Box(rectObj, "# UDP Object Receive\n127.0.0.1:" + portLocal + "\n"
		+ "\nLast Packet: \n" + lastReceivedUDPPacket
		+ "\n\nAll Messages: \n" + allReceivedUDPPackets
			, style);

		strMessageSend = GUI.TextField(new Rect(40, 420, 140, 20), strMessageSend);
		if (GUI.Button(new Rect(190, 420, 40, 20), "send"))
		{
			sendData(strMessageSend + "\n");
		}

	}

	// Initialization code
	private void init()
	{
		// Initialize (seen in comments window)
		//print("UDP Object init()");

		// Create remote endpoint (to Matlab) 
		remoteEndPoint = new IPEndPoint(IPAddress.Parse(IP), portRemote);

		// Create local client
		client = new UdpClient(portLocal);

		// local endpoint define (where messages are received)
		// Create a new thread for reception of incoming messages
		receiveThread = new Thread(
			new ThreadStart(ReceiveData));
		receiveThread.IsBackground = true;
		receiveThread.Start();
	}

	// Receive data, update packets received
	private void ReceiveData()
	{
		while (true)
		{

			try
			{
				IPEndPoint anyIP = new IPEndPoint(IPAddress.Any, 0);
				byte[] data = client.Receive(ref anyIP);
				string text = Encoding.UTF8.GetString(data);
				//print (">> " + text);
				lastReceivedUDPPacket = text;
				allReceivedUDPPackets = allReceivedUDPPackets + text;

			}
			catch (Exception err)
			{
				//print (err.ToString());
			}
		}
	}

	// Send data
	public void sendData(string message)
	{
		try
		{
			//byte[] data = Encoding.UTF8.GetBytes(message);
			byte[] data = BitConverter.GetBytes(Convert.ToDouble(message));
			client.Send(data, data.Length, remoteEndPoint);
			//sb.Append(ToReadableByteArray(data) + '\n');
			//File.AppendAllText("C:\\Users\\ricky\\Desktop\\NEU\\" + "log.txt", sb.ToString());
			//sb.Clear();
		}
		catch (Exception err)
		{
			//print (err.ToString());
		}
	}

	static public string ToReadableByteArray(byte[] bytes)
	{
		return string.Join(", ", bytes);
	}

	public void sendData(Vector3 message)
	{
		try
		{
			byte[] dataX = BitConverter.GetBytes(Convert.ToDouble(message.x));
			client.Send(dataX, dataX.Length, remoteEndPoint);
			byte[] dataY = BitConverter.GetBytes(Convert.ToDouble(message.y));
			client.Send(dataY, dataY.Length, remoteEndPoint);
			byte[] dataZ = BitConverter.GetBytes(Convert.ToDouble(message.z));
			client.Send(dataZ, dataZ.Length, remoteEndPoint);
			sb.Append(Convert.ToString(message.x) + '\n');
			sb.Append(Convert.ToString(message.y) + '\n');
			sb.Append(Convert.ToString(message.z) + '\n');
			File.AppendAllText("C:\\Users\\ricky\\Desktop\\NEU\\Robotics Science and Systems\\Project\\" + "log.txt", sb.ToString());
			sb.Clear();

		}
		catch (Exception err)
		{
			//print(err.ToString());
		}
	}

	public void sendData(int message)
	{
		try
		{
			byte[] data = BitConverter.GetBytes(message);
			client.Send(data, data.Length, remoteEndPoint);
			Debug.Log(data.Length);
		}
		catch (Exception err)
		{
			//print(err.ToString());
		}
	}

	// getLatestUDPPacket, clears all previous packets
	public string getLatestUDPPacket()
	{
		allReceivedUDPPackets = "";
		return lastReceivedUDPPacket;
	}

	//Prevent crashes - close clients and threads properly!
	void OnDisable()
	{
		if (receiveThread != null)
			receiveThread.Abort();

		client.Close();

	}

}